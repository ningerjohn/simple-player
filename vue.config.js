// vue.config.js
const webpack = require('webpack');

module.exports = {
    pluginOptions: {
        electronBuilder: {
            // List native deps here if they don't work
            externals: ['ffmpeg-static', 'ffprobe-static'],
            nodeModulesPath: ['../../node_modules', './node_modules'],
            nodeIntegration: true,
            builderOptions: {
                // options placed here will be merged with default configuration and passed to electron-builder
                "nsis": {
                    "oneClick": false,
                    "perMachine": true,
                    "allowToChangeInstallationDirectory": true
                },
                "files": "!node_modules/ffprobe-static/**/*",
                "win": {
                    "target": "portable",
                    "extraResources": {
                        "from": "node_modules/ffprobe-static/bin/win32/x64"
                    }
                }
            },
        }
    },
    configureWebpack: {
        plugins: [
            new webpack.DefinePlugin({
                'process.env.FLUENTFFMPEG_COV': false // https://github.com/fluent-ffmpeg/node-fluent-ffmpeg/issues/998#issuecomment-643605939
            })
        ]
    }
}
