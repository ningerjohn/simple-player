'use strict'

import { app, protocol, BrowserWindow, Menu, ipcMain, dialog } from 'electron'
import { createProtocol, installVueDevtools } from 'vue-cli-plugin-electron-builder/lib'

import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer';

import { videoSupport } from './main/ffmpeg-helper';
import VideoServer from './main/VideoServer';

const isDevelopment = process.env.NODE_ENV !== 'production'

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win
let httpServer;
let isRendererReady = false;

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([{ scheme: 'app', privileges: { secure: true, standard: true } }])

function onMediaFileChange(videoFilePath) {
    videoSupport(videoFilePath).then((checkResult) => {
        // if (checkResult.videoCodecSupport && checkResult.audioCodecSupport) { // 支持播放
        //     if (httpServer) {
        //         httpServer.killFfmpegCommand();
        //     }
        //     let playParams = {};
        //     playParams.type = "native";
        //     playParams.videoSource = videoFilePath;
        //     if (isRendererReady) {
        //         console.log("fileSelected=", playParams)
        //         win.webContents.send('fileSelected', playParams);
        //     } else {
        //         ipcMain.once("ipcRendererReady", (event, args) => {
        //             console.log("fileSelected", playParams)
        //             win.webContents.send('fileSelected', playParams);
        //             isRendererReady = true;
        //         })
        //     }
        // }
        if (!checkResult.videoCodecSupport || !checkResult.audioCodecSupport) { // 不支持播放
            if (!httpServer) {
                httpServer = new VideoServer();
            }
            httpServer.videoSourceInfo = { videoSourcePath: videoFilePath, checkResult: checkResult };
            httpServer.createServer();
            console.log("createVideoServer success", new Date().getTime());
            let playParams = {};
            playParams.type = "stream";
            playParams.videoSource = "http://127.0.0.1:7777?startTime=0";
            playParams.duration = checkResult.duration
            if (isRendererReady) {
                console.log("fileSelected=", playParams)
                win.webContents.send('fileSelected', playParams);
            } else {
                ipcMain.once("ipcRendererReady", (event, args) => {
                    console.log("fileSelected", playParams)
                    win.webContents.send('fileSelected', playParams);
                    isRendererReady = true;
                })
            }
        }
    }).catch((err) => {
        console.log("video format error", err);
        const options = {
            type: 'info',
            title: 'Error',
            message: "It is not a video file!",
            buttons: ['OK']
        }
        // dialog.showMessageBox(options, function (index) {
        //     console.log("showMessageBox", index);
        // })
    })
}

function createWindow() {
    // Create the browser window.
    // 隐藏菜单栏
    Menu.setApplicationMenu(null)
    win = new BrowserWindow({
        width: 1400, // 1200
        height: 900, // 700
        frame: false, // 无边框
        // transparent: true,
        // backgroundColor: '#00000000',
        autoHideMenuBar: true,
        title: 'Simple Video Player',
        webviewTag: true,
        webPreferences: {
            nodeIntegration: true, // 允许使用node的模块，比如fs
            webSecurity: false // 允许本地file协议
        }
    })

    if (process.env.WEBPACK_DEV_SERVER_URL) {
        // Load the url of the dev server if in development mode
        win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
        if (!process.env.IS_TEST || true) win.webContents.openDevTools() // open dev tools
    } else {
        createProtocol('app')
        // Load the index.html when not in development
        win.loadURL('app://./index.html')
    }

    win.on('closed', () => {
        win = null
    })
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
    if (isDevelopment && !process.env.IS_TEST) {
        // Install Vue Devtools
        // Devtools extensions are broken in Electron 6.0.0 and greater
        // See https://github.com/nklayman/vue-cli-plugin-electron-builder/issues/378 for more info
        // Electron will not launch with Devtools extensions installed on Windows 10 with dark mode
        // If you are not using Windows 10 dark mode, you may uncomment these lines
        // In addition, if the linked issue is closed, you can upgrade electron and uncomment these lines
        // try {
        //     await installVueDevtools()
        // } catch (e) {
        //     console.error('Vue Devtools failed to install:', e.toString())
        // }
        installExtension(VUEJS_DEVTOOLS)
            .then((name) => console.log(`Added Extension:  ${name}`))
            .catch((err) => console.log('An error occurred: ', err));
    }
    createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
    if (process.platform === 'win32') {
        process.on('message', data => {
            if (data === 'graceful-exit') {
                app.quit()
            }
        })
    } else {
        process.on('SIGTERM', () => {
            app.quit()
        })
    }
}

// 定义你如果要关掉软件，那么在这里申明!
ipcMain.on('close-window', function () {
    win.close();
})
//小化
ipcMain.on('hide-window', () => {
    win.minimize();
});
//最大化
ipcMain.on('show-window', () => {
    win.maximize();
});
//还原
ipcMain.on('orignal-window', () => {
    win.unmaximize();
});

ipcMain.on('player:playMediaWithServer', (event, arg) => {
    console.log("player:playMediaWithServer:", arg);
    onMediaFileChange(arg);
});
