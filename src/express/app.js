// https://betterprogramming.pub/video-stream-with-node-js-and-html5-320b3191a6b6
// https://blog.csdn.net/GISuuser/article/details/79140045
var express = require('express');
var app = express();
var fs = require('fs');
import mime from 'mime'

const server = app.get('/video', function (req, res) {
    let filePath = './src/express/1.mp4'
    console.log(req.query.file);
    const queryFilePath = req.query.file
    if ( queryFilePath ) {
        filePath = queryFilePath
    }
    const stat = fs.statSync(filePath)
    console.log(stat);
    const mimeType = mime.getType(filePath)
    const fileSize = stat.size
    const range = req.headers.range
    if (range) {
        const parts = range.replace(/bytes=/, "").split("-")
        const start = parseInt(parts[0], 10)
        const end = parts[1]
            ? parseInt(parts[1], 10)
            : fileSize - 1
        const chunksize = (end - start) + 1
        const file = fs.createReadStream(filePath, { start, end })
        const head = {
            'Content-Range': `bytes ${start}-${end}/${fileSize}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': chunksize,
            'Content-Type': mimeType,
        }
        res.writeHead(206, head);
        file.pipe(res);
    } else {
        const head = {
            'Content-Length': fileSize,
            'Content-Type': mimeType,
        }
        res.writeHead(200, head)
        fs.createReadStream(filePath).pipe(res)
    }
}).listen(3000);

