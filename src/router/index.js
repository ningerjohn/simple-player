import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Layout',
        component: () => import('../views/layouts/Default.vue'),
        redirect: '/index',
        children: [
            { path: '/index', name: 'Index', component: () => import('../views/Index.vue') },
            { path: '/video', name: 'Video', component: () => import('../views/Video.vue') },
            { path: '/music', name: 'Audio', component: () => import('../views/Audio.vue') },
            { path: '/download', name: 'Download', component: () => import('../views/Download.vue') },
        ]
    },
    // {
    //     path: '/',
    //     name: 'Video',
    //     component: () => import('../views/Video.vue')
    // }
]

const router = new VueRouter({
    routes
})

export default router
