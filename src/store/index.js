import Vue from 'vue'
import Vuex from 'vuex'
import fs from 'fs'
import path from 'path'
import mime from 'mime'
import ffprobe from 'ffprobe'
import ffprobeStatic from 'ffprobe-static'
import createPersistedState from 'vuex-persistedstate'
// import { Buffer } from 'buffer';

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        selectedFolder: [],
        mediaList: [],
        crtMediaInfo: {},
        crtMediaPath: null,
        crtMediaPlayingSource: null,
        playStatus: false, // 默认暂停状态
    },
    getters: {
        selectedFolderData: state => {
            console.log(state.selectedFolder)
            return state.selectedFolder
        },
        videoList: state => {
            return state.mediaList.filter(item => item.type === 'video')
        },
        musicList: state => {
            return state.mediaList.filter(item => item.type === 'music')
        }
    },
    mutations: {
        forceChangeVideoPath: function (state, path) {
            state.crtMediaPath = path
        },
        togglePlayStatus: function (state, status) {
            state.playStatus = status
        },
        selectCrtMediaData: function (state, item) {
            state.playStatus = false
            state.crtMediaInfo = item
            if (item.type === 'video' || item.type === 'music') {
                state.crtMediaPath = item.filePath
            }
        },
        updateCrtMediaPath: function (state, newVal) {
            state.crtMediaPath = newVal
        },
        storeFolder: function (state, newFolder) {
            state.selectedFolder.push({ folderPath: newFolder, addTime: new Date().getTime() })
        },
        deleteFolder: function (state, targetfolderPath) {
            const temp = state.selectedFolder.filter(({ folderPath }) => folderPath !== targetfolderPath)
            console.log(temp);
            state.selectedFolder = temp
        },
        clearAllListData: function (state) {
            state.mediaList = []
        },
        updateListData: function (state, data) {
            const getFileSize = (a, decimals = 2) => {
                if (a === 0) { return "0 Bytes" }
                const c = 1024
                const d = decimals || 2
                const e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
                const f = Math.floor(Math.log(a) / Math.log(c));
                return parseFloat((a / Math.pow(c, f)).toFixed(d)) + " " + e[f]
            }
            const { filePath, fileSize, metaInfo } = data
            // 多媒体基本物理信息
            let fileObj = {
                title: path.basename(filePath, path.extname(filePath)),
                ext: path.extname(filePath),
                filePath: filePath,
                mimeType: mime.getType(filePath),
                fileSize: getFileSize(fileSize),
                duration: 0
            }
            fileObj = Object.assign(fileObj, {}) // tagInfo
            // 多媒体
            let isMedia = false
            if (fileObj.mimeType.indexOf('video') >= 0) {
                isMedia = true
                const [videoMeta, audioMeta] = metaInfo.streams
                fileObj = Object.assign(fileObj, { type: 'video', videoMeta: videoMeta, audioMeta: audioMeta }) // tagInfo
            } else if (fileObj.mimeType.indexOf('audio') >= 0) {
                isMedia = true
                const [audioMeta] = metaInfo.streams
                fileObj = Object.assign(fileObj, { type: 'music', audioMeta: audioMeta }) // tagInfo
            }
            if (isMedia) {
                state.mediaList.push(fileObj)
            }
        },
        updateMusicMetaData: function (state, data) {
            const { filePath, metadata } = data
            for (const [key, item] of Object.entries(state.mediaList)) {
                if (item.filePath === filePath) {
                    state.mediaList[key].metadata = metadata
                    state.mediaList[key].duration = metadata.format.duration
                }
            }
            state.mediaList = [...state.mediaList]
        }
    },
    actions: {
        resursiveSelectedFolder(context) {
            for (let index = 0; index < context.state.selectedFolder.length; index++) {
                const directory = context.state.selectedFolder[index].folderPath;
                context.dispatch('readDirectory', directory)
            }
        },
        readDirectory(context, targetDir) { // 读取目录
            context.commit('clearAllListData')
            fs.readdir(targetDir, function (err, files) { // 根据文件路径读取文件，返回文件列表
                if (err) {
                    console.warn(err)
                } else {
                    // 遍历读取到的文件列表
                    files.forEach(function (filename) {
                        // 获取当前文件的绝对路径
                        var filePath = path.join(targetDir, filename);
                        // 根据文件路径获取文件信息，返回一个fs.Stats对象
                        fs.stat(filePath, function (eror, stats) {
                            if (eror) {
                                console.warn('获取文件stats失败');
                            } else {
                                if (stats.isFile()) { // 是文件
                                    const fileSize = stats.size
                                    // ffprobe metadata
                                    let ffprobeStaticPath = ''
                                    if (!__dirname.includes('.asar')) { // If dev
                                        ffprobeStaticPath = ffprobeStatic.path
                                    } else { // if compiled
                                        let ext = ''
                                        if (process.platform === 'win32') ext = '.exe' // if windows
                                        ffprobeStaticPath = path.join(process.resourcesPath + '/ffprobe' + ext)
                                    }
                                    ffprobeStaticPath = 'D:\\www\\learn\\simple-player\\node_modules\\ffprobe-static\\bin\\win32\\x64\\ffprobe.exe'
                                    ffprobe(filePath, { path: ffprobeStaticPath })
                                        .then((metaInfo) => {
                                            context.commit('updateListData', { filePath, fileSize, metaInfo });
                                            // music metadata
                                            const mm = require('music-metadata');
                                            // const util = require('util');
                                            (async () => {
                                                try {
                                                    const metadata = await mm.parseFile(filePath);
                                                    context.commit('updateMusicMetaData', { filePath, metadata })
                                                } catch (error) {
                                                    // console.error(filePath, error.message);
                                                }
                                            })();
                                        })
                                    // // medadata-extract
                                    // const extract = require('metadata-extract')
                                    // const metadata = extract(filePath, { extractors: ['music-metadata.js'] })
                                    // console.log(metadata.then((a) => {
                                    //     console.log(a);
                                    // }));
                                } else if (stats.isDirectory()) { // 是文件夹
                                    context.dispatch('readDirectory', filePath);// 递归，如果是文件夹，就继续遍历该文件夹下面的文件
                                }
                            }
                        })
                    });
                }
            })
        }
    },
    modules: {
    }
})
